if exists('g:loaded_syntastic_markdown_checknotes_checker')
    finish
endif
let g:loaded_syntastic_markdown_checknotes_checker = 1

let s:save_cpo = &cpo
set cpo&vim

let s:path = expand('<sfile>:p:h')

if exists('g:syntastic_extra_filetypes')
    call add(g:syntastic_extra_filetypes, 'markdown')
else
    let g:syntastic_extra_filetypes = ['markdown']
endif

function! SyntaxCheckers_markdown_checknotes_IsAvailable() dict
    return executable(self.getExec())
endfunction

function! SyntaxCheckers_markdown_checknotes_GetLocList() dict
    let makeprg = self.makeprgBuild({
                \ 'args': '--project',
                \ 'args_after': '' })

    let errorformat = '%E%f (%l:%c): %m'

    return SyntasticMake({ 'makeprg': makeprg, 'errorformat': errorformat})
endfunction

call g:SyntasticRegistry.CreateAndRegisterChecker({
            \ 'filetype': 'markdown',
            \ 'name': 'checknotes' })

let &cpo = s:save_cpo
unlet s:save_cpo

" vim: set sw=4 sts=4 et fdm=marker:
